import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Importamos los componentes que usaremos para implementar el sidebar menu
import { NewleftmenuComponent } from './components/newleftmenu/newleftmenu.component';
import { NewsidebarComponent } from './components/newsidebar/newsidebar.component';
// Importamos los módulos que usaremos en la aplicación

import { TieredMenuDemoModule } from './components/tieredmenu/tieredmenudemo.module';
//import { SlideMenuDemoModule } from './components/slidemenu/slidemenudemo.module';
import { SidebarDemoModule } from './components/sidebar/sidebardemo.module';

@NgModule({
  declarations: [
    NewleftmenuComponent,
    NewsidebarComponent
  ],
  imports: [
    CommonModule,
    TieredMenuDemoModule
  ],
  exports: [
  	SidebarDemoModule
  ]
})
export class LeftmenuModule { }
