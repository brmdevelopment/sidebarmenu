import { Component } from '@angular/core';
import { MenuItem } from 'primeng/components/common/api';

@Component({
    selector: 'app-tieredmenu',
    templateUrl: './tieredmenudemo.html',
    styleUrls: ['./tieredmenudemo.component.css']
})
export class TieredMenuDemo {
    
    items: MenuItem[];
    mainmenu: "main-menu";

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                styleClass: 'nueva',
                icon: 'pi pi-fw pi-file',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'Project'},
                            {label: 'Other'},
                        ]
                    },
                    {label: 'Open'},
                    {separator:true},
                    {label: 'Quit'}
                ]
            }
        ];
    }
}