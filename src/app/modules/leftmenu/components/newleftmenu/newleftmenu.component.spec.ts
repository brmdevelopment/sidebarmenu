import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewleftmenuComponent } from './newleftmenu.component';

describe('NewleftmenuComponent', () => {
  let component: NewleftmenuComponent;
  let fixture: ComponentFixture<NewleftmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewleftmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewleftmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
