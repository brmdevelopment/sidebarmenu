import {NgModule}     from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule}    from '@angular/forms';
import {SidebarDemo} from './sidebardemo';
import {SidebarDemoRoutingModule} from './sidebardemo-routing.module';

import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from 'primeng/button';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import { TieredMenuDemoModule } from '../tieredmenu/tieredmenudemo.module';

@NgModule({
	imports: [
		CommonModule,
		SidebarDemoRoutingModule,
        FormsModule,
        SidebarModule,
        ButtonModule,
        TabViewModule,
        CodeHighlighterModule,
        TieredMenuDemoModule
	],
	declarations: [
		SidebarDemo
	]
})
export class SidebarDemoModule {}
