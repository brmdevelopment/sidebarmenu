import {Component} from '@angular/core';

@Component({
    templateUrl: './sidebardemo.html',
    styleUrls: ['./sidebardemo.component.css']
})
export class SidebarDemo {

    visibleSidebar1;
    
    visibleSidebar2;
    
    visibleSidebar3;
    
    visibleSidebar4;
    
    visibleSidebar5;
}