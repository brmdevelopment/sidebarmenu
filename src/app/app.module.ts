import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

//Modules style
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion';
import {MessagesModule} from 'primeng/primeng';
import {MessageModule} from 'primeng/primeng';

//Modules Components
import { SlideMenuDemoModule } from './modules/leftmenu/components/slidemenu/slidemenudemo.module';
import { LeftmenuModule } from './modules/leftmenu/leftmenu.module';
//import { TieredMenuDemoModule } from './modules/leftmenu/components/tieredmenu/tieredmenudemo.module';
//import { SidebarDemoModule } from './modules/leftmenu/components/sidebar/sidebardemo.module';

//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/general/login/login.component';
import {AuthGuard} from './guards/auth.guard';
import {UserSessionService} from './services/general/user-session.service';
import { DasboardComponent } from './components/general/dasboard/dasboard.component';
import {LeftMenuComponent} from './components/general/left-menu/left-menu.component';
//import { LeftMenuComponent } from './components/general/left-menu/left-menu.component';
//import { TieredMenuDemo } from './components/tieredmenu/tieredmenudemo';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LeftMenuComponent,
    DasboardComponent,
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ButtonModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    SlideMenuDemoModule,
    //TieredMenuDemoModule,
    //SidebarDemoModule,
    LeftmenuModule
  ],

  providers: [
    AuthGuard,
    UserSessionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
