import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';
import {environment } from '../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'X-DreamFactory-API-Key': environment.API_KEY_LDAP, 'Content-Type': 'application/json', 'X-DreamFactory-Session-Token': localStorage.getItem('X-Token-Mind')}
  )};

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  public API_URL = environment.API_URL;

  constructor(private http: HttpClient) { }

  getDataUser(){
    return this.http.get(this.API_URL+'user/session', httpOptions);
  }

  getDataUserLDAP(){
    return this.http.get(this.API_URL+'Directorio_BRM/user/'+localStorage.getItem('X-Username-Mind'), httpOptions);
  }
}
