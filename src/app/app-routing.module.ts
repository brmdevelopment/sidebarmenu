import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { LeftMenuComponent } from './components/general/left-menu/left-menu.component';
import { TieredMenuDemo } from './modules/leftmenu/components/tieredmenu/tieredmenudemo';
import { SidebarDemo } from './modules/leftmenu/components/sidebar/sidebardemo';
import { NewsidebarComponent } from './modules/leftmenu/components/newsidebar/newsidebar.component';
import {LoginComponent} from './components/general/login/login.component';
import {DasboardComponent} from './components/general/dasboard/dasboard.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'mind', component: LeftMenuComponent, canActivate: [AuthGuard],
    children: [
      {
        path: '', component: DasboardComponent
      }
    ] },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'side', component: SidebarDemo
  },
  {
    path: 'tiered', component: TieredMenuDemo
  },
  {
    path: 'slide', component: LeftMenuComponent
  }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],

  exports: [RouterModule]
})
export class AppRoutingModule { }
