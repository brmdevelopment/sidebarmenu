import { Component, OnInit } from '@angular/core';
import {UserSessionService} from '../../../services/general/user-session.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {
  public token: string;
  public ldap: any;
  public user: any;
  public username: string;
  public userrol: string;


  constructor(
    private session_service: UserSessionService,
    private router: Router,
    protected activatedRoute: ActivatedRoute
  ) {

   }

  ngOnInit() {
    console.log('home');
    this.session_service.getDataUser().subscribe(
      result => {
        this.user = result;
        //console.log(this.user.cn);
        if (!this.user.session_token){
          localStorage.clear();
          this.router.navigate(['login']);
        }else{
          this.getInfoUserLDAP();
        }
      }, error => {
        localStorage.clear();
        this.router.navigate(['login']);
        console.log(<any>error.error);
      }
    );
  }

  getInfoUserLDAP(){
    this.session_service.getDataUserLDAP().subscribe(
      result => {
        this.ldap = result;
        this.username = this.ldap.cn;
        this.userrol = this.ldap.memberof[2];

        console.log(
          this.ldap,
          this.username, 
          this.userrol);
      
      }, error => {
        console.log(<any>error.error.error);
      }
    );
  }

}
