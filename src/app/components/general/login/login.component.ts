import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { User } from '../../../models/general/user';
import {AuthService} from '../../../services/general/auth.service';
import {Message} from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {

  public user: User;
  public token: string;
  msgs: Message[] = [];
  constructor(
    private  auth_service: AuthService,
    private  router: Router
  ) {
    this.user = new User();
  }

  ngOnInit() {
    this.token = localStorage.getItem('X-Token-Mind');
    if (this.token){
      this.router.navigate(['mind']);
    }
  }

  onSubmit(){
    this.user.service="Directorio_BRM";
    console.log(this.user);
    this.auth_service.addUser(this.user).subscribe(
      result => {
        localStorage.setItem('X-Token-Mind', result.session_token);
        localStorage.setItem('X-Username-Mind', this.user.username);
        location.reload();
      }, error => {
    /*    this.msgs=<any>error.error.error.message;*/
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Error', detail: error.error.error.message});
        console.log(<any>error.error);
      }
    );
  }

}
