import { Component } from '@angular/core';
import { MenuItem } from 'primeng/components/common/api';
import { TieredMenuDemo } from '../tieredmenu/tieredmenudemo';

@Component({
    templateUrl: './slidemenudemo.html',
    styleUrls: ['./slidemenudemo.component.css']
})
export class SlideMenuDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                icon: 'pi pi-fw pi-file',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'Project'},
                            {label: 'Other'},
                        ]
                    },
                    {label: 'Open'},
                    {separator:true},
                    {label: 'Quit'}
                ]
            }
        ];
    }
}