import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public  token: string;
  constructor( private router: Router){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.token=localStorage.getItem('X-Token-Mind');
    console.log(localStorage.key(0));
    if (this.token){
      return true;
    }

    if (!this.token){
      localStorage.clear();
      const link = ['login'];
      this.router.navigate(link);
      return false;
    }
  }
}
